# react-query

react-query is a javascript library to combine css-selector with react fiber internal implementation. The purpose is to provide a tool to query a React page by React Component names instead of relying on CSS/DOM selector.

# Usage

For now, have a look at test file to see the usage
