import * as CSSWhat from 'css-what';
import Utils from './react_utils';

function encode(query) {
  return query.replace('.', '_____')
}

function decode(query) {
  return query.replace('_____', '.')
}

function parseQuery(query) {
  // Ignore select by css (so we can use name with '.')
  query = encode(query)
  return CSSWhat.parse(query, { lowerCaseAttributeNames: false, lowerCaseTags: false });
}

// Current design is not intended to be optimized or fast per-ser. We'll always travel the whole tree to find
const handlers = {
  'tag': ({ name }, sources) => {
    // const { currentIDs, descriptions } = describeFibers(node);
    let result = [];
    name = decode(name)

    function matchByName(fiber) {
      return fiber.type && (
        fiber.type.name === name ||
        fiber.type.displayName === name ||
        fiber.type.toString() === name)
    }

    function findIn(fiber) {
      if (matchByName(fiber)) {
        result.push(fiber);
      } else {
        fiber.child && findIn(fiber.child)
      }

      fiber.sibling && findIn(fiber.sibling)
    }

    sources.forEach(findIn);
    return result;
  },
  'pseudo': ({ name, data }, sources) => {
    if (sources.length === 0) return sources;

    switch (name) {
      case 'first': return [sources[0]]
      case 'last': return [sources[sources.length - 1]]
      case 'skip': return sources.slice(data)
      case 'nth': return sources[data] ? [sources[data]] : []
      default: throw Error(`:${name} is unsupported`)
    }
  },
  'attribute': ({ name, action, value }, sources) => {
    let result = [];
    value = value.toString();

    for (let source of sources) {
      const memoizedProps = source.memoizedProps;
      switch (action) {
        case 'exists': memoizedProps[name] && result.push(source); break;
        case 'equals': `${memoizedProps[name]}` === value && result.push(source); break;
        case 'start': `${memoizedProps[name]}`.startsWith(value) && result.push(source); break;
        case 'end': `${memoizedProps[name]}`.endsWith(value) && result.push(source); break;
        default: throw Error(`[${action}] is an unsupported`)
      }
    }
    return result;
  },
  'child': (_, sources) => {
    let result = [];
    function findSibling(fiber) {
      if (fiber.sibling) {
        result.push(fiber.sibling)
        findSibling(fiber.sibling)
      }
    }

    for (let source of sources) {
      if (source.child) {
        result.push(source.child)
        findSibling(source.child)
      }
    }

    return result
  },
  'parent': (_, sources) => {
    return sources.map(source => source.return)
  },
  'sibling': (_, sources) => {
    let result = [];

    function findSibling(fiber) {
      if (fiber.sibling) {
        result.push(fiber.sibling)
        findSibling(fiber.sibling)
      }
    }

    for (let source of sources) {
      findSibling(source)
    }
    return result;
  },
  'adjacent': (_, sources) => {
    let result = [];
    for (let source of sources) {
      if (source.sibling) result.push(source.sibling)
    }
    return result;
  },
  'descendant': (_, sources) => handlers['child'](_, sources)
}


function _query(queries, node) {
  let result = [node];

  for (let query of queries) {
    result = [...new Set(handlers[query.type](query, result))];

  }

  return result.map(child => Utils.findDOMComponent(child));
}

export default function query(rootNode, queryString, debug) {
  if (!rootNode) {
    throw new Error("rootNode is not given")
  }

  if (!queryString || typeof queryString != 'string') {
    throw new Error("queryString must be a string")
  }

  if (!Utils.isRootNode(rootNode) && !Utils.isReactNode(rootNode)) {
    throw new Error("given rootNode is not a React node");
  }

  const parsedQuery = parseQuery(queryString)[0];
  const reactNode = Utils.isRootNode(rootNode) || Utils.isReactNode(rootNode);

  return _query(parsedQuery, reactNode);
}