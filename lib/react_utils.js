import { HostComponent } from './react_internal'

const findDOMComponent = (node) => {
  if (node.tag === HostComponent) {
    return node.stateNode;
  }

  if (node.child) {
    if (node.child.tag === HostComponent) {
      return node.child.stateNode;
    }

    return findDOMComponent(node.child);
  }
}

export default {
  findDOMComponent,
  isRootNode: (node) => {
    return node._reactRootContainer
      && node._reactRootContainer._internalRoot
      && node._reactRootContainer._internalRoot.current;
  },
  isReactNode: (node) => {
    return Object.keys(node).find(key => key.startsWith('__reactInternalInstance'))
  }
}