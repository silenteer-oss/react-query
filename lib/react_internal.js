let nextFiberID = 1;
const fiberIDMap = new WeakMap();

const FunctionComponent = 0;
const ClassComponent = 1;
const IndeterminateComponent = 2; // Before we know whether it is function or class
const HostRoot = 3; // Root of a host tree. Could be nested inside another node.
const HostPortal = 4; // A subtree. Could be an entry point to a different renderer.
export const HostComponent = 5;
const HostText = 6;
const Fragment = 7;
const Mode = 8;
const ContextConsumer = 9;
const ContextProvider = 10;
const ForwardRef = 11;
const Profiler = 12;
const SuspenseComponent = 13;
const MemoComponent = 14;
const SimpleMemoComponent = 15;
const LazyComponent = 16;
const IncompleteClassComponent = 17;
const DehydratedFragment = 18;
const SuspenseListComponent = 19;
const FundamentalComponent = 20;
const ScopeComponent = 21;
const Block = 22;

function getFiberUniqueID(fiber) {
  if (!fiberIDMap.has(fiber)) {
    fiberIDMap.set(fiber, nextFiberID++);
  }
  return fiberIDMap.get(fiber);
}

function getFriendlyTag(tag) {
  switch (tag) {
    case IndeterminateComponent:
      return '[indeterminate]';
    case FunctionComponent:
      return '[fn]';
    case ClassComponent:
      return '[class]';
    case HostRoot:
      return '[root]';
    case HostPortal:
      return '[portal]';
    case HostComponent:
      return '[host]';
    case HostText:
      return '[text]';
    case Fragment:
      return '[fragment]';
    case Mode:
      return '[mode]';
    case ContextConsumer:
      return '[consumer]';
    case ContextProvider:
      return '[provider]';
    case ForwardRef:
      return '[forwardref]'
    case Profiler:
      return '[profiler]';
    case SuspenseComponent:
      return '[suspense]';
    case MemoComponent:
      return '[memo]';
    case SimpleMemoComponent:
      return '[simplememo]'
    case LazyComponent:
      return '[lazy]'
    case IncompleteClassComponent:
      return '[incompleteclasscomponent]'
    case DehydratedFragment:
      return '[dehydrated]'
    case SuspenseListComponent:
      return '[suspenselist]'
    case FundamentalComponent:
      return '[fundamentalcomponent]'
    case ScopeComponent:
      return '[scopecomponent]'
    case Block:
      return '[block]'
    default:
      throw new Error('Unknown tag.');
  }
}

function getFriendlyEffect(effectTag) {
  const effects = {
    1: 'Performed Work',
    2: 'Placement',
    4: 'Update',
    8: 'Deletion',
    16: 'Content reset',
    32: 'Callback',
    64: 'Err',
    128: 'Ref',
  };
  return Object.keys(effects)
    .map(Number)
    .filter(flag => flag & effectTag)
    .map(flag => effects[flag])
    .join(' & ');
}

function describeFibers(rootFiber, workInProgress) {
  let descriptions = {};
  function acknowledgeFiber(fiber) {
    if (!fiber) {
      return null;
    }
    if (!fiber.return && fiber.tag !== 3) {
      return null;
    }
    const id = getFiberUniqueID(fiber);
    if (descriptions[id]) {
      return id;
    }

    descriptions[id] = {};
    Object.assign(descriptions[id], {
      ...fiber,
      _desc: {
        id: id,
        tag: getFriendlyTag(fiber.tag),
        effectTag: getFriendlyEffect(fiber.effectTag),
        type: fiber.type && (fiber.type.name || fiber.type.toString()),
        stateNode: `[${typeof fiber.stateNode}]`,
        return: acknowledgeFiber(fiber.return),
        child: acknowledgeFiber(fiber.child),
        sibling: acknowledgeFiber(fiber.sibling),
        nextEffect: acknowledgeFiber(fiber.nextEffect),
        firstEffect: acknowledgeFiber(fiber.firstEffect),
        lastEffect: acknowledgeFiber(fiber.lastEffect),
        alternate: acknowledgeFiber(fiber.alternate),
      }
    });
    return id;
  }

  const rootID = acknowledgeFiber(rootFiber);
  const workInProgressID = acknowledgeFiber(workInProgress);

  let currentIDs = [];
  function markAsCurrent(id) {
    currentIDs.push(id);
    const fiber = descriptions[id];
    if (!fiber) {
      console.log(descriptions)
      throw new Error(`Could not find fiber ${id}`)
    }

    if (fiber.child) {
      markAsCurrent(fiber.child);
    }

    if (fiber.sibling) {
      markAsCurrent(fiber.sibling);
    }
  }
  markAsCurrent(rootID);

  return {
    descriptions,
    rootID,
    currentIDs,
    workInProgressID,
  };
}

export default describeFibers;