var rquery = (function () {
	'use strict';

	function unwrapExports (x) {
		return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
	}

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var parse_1 = createCommonjsModule(function (module, exports) {
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = parse;
	var reName = /^(?:\\([\da-f]{1,6}\s?|(\s)|.)|[\w\-\u00b0-\uFFFF])+/, reEscape = /\\([\da-f]{1,6}\s?|(\s)|.)/gi, 
	//modified version of https://github.com/jquery/sizzle/blob/master/src/sizzle.js#L87
	reAttr = /^\s*((?:\\.|[\w\u00b0-\uFFFF-])+)\s*(?:(\S?)=\s*(?:(['"])([^]*?)\3|(#?(?:\\.|[\w\u00b0-\uFFFF-])*)|)|)\s*(i)?\]/;
	var actionTypes = {
	    undefined: "exists",
	    "": "equals",
	    "~": "element",
	    "^": "start",
	    $: "end",
	    "*": "any",
	    "!": "not",
	    "|": "hyphen"
	};
	var Traversals = {
	    ">": "child",
	    "<": "parent",
	    "~": "sibling",
	    "+": "adjacent"
	};
	var attribSelectors = {
	    "#": ["id", "equals"],
	    ".": ["class", "element"]
	};
	//pseudos, whose data-property is parsed as well
	var unpackPseudos = new Set(["has", "not", "matches"]);
	var stripQuotesFromPseudos = new Set(["contains", "icontains"]);
	var quotes = new Set(['"', "'"]);
	//unescape function taken from https://github.com/jquery/sizzle/blob/master/src/sizzle.js#L152
	function funescape(_, escaped, escapedWhitespace) {
	    var high = parseInt(escaped, 16) - 0x10000;
	    // NaN means non-codepoint
	    return high !== high || escapedWhitespace
	        ? escaped
	        : high < 0
	            ? // BMP codepoint
	                String.fromCharCode(high + 0x10000)
	            : // Supplemental Plane codepoint (surrogate pair)
	                String.fromCharCode((high >> 10) | 0xd800, (high & 0x3ff) | 0xdc00);
	}
	function unescapeCSS(str) {
	    return str.replace(reEscape, funescape);
	}
	function isWhitespace(c) {
	    return c === " " || c === "\n" || c === "\t" || c === "\f" || c === "\r";
	}
	function parse(selector, options) {
	    var subselects = [];
	    selector = parseSelector(subselects, selector + "", options);
	    if (selector !== "") {
	        throw new Error("Unmatched selector: " + selector);
	    }
	    return subselects;
	}
	function parseSelector(subselects, selector, options) {
	    var tokens = [], sawWS = false;
	    function getName() {
	        var match = selector.match(reName);
	        if (!match) {
	            throw new Error("Expected name, found " + selector);
	        }
	        var sub = match[0];
	        selector = selector.substr(sub.length);
	        return unescapeCSS(sub);
	    }
	    function stripWhitespace(start) {
	        while (isWhitespace(selector.charAt(start)))
	            start++;
	        selector = selector.substr(start);
	    }
	    function isEscaped(pos) {
	        var slashCount = 0;
	        while (selector.charAt(--pos) === "\\")
	            slashCount++;
	        return (slashCount & 1) === 1;
	    }
	    stripWhitespace(0);
	    while (selector !== "") {
	        var firstChar = selector.charAt(0);
	        if (isWhitespace(firstChar)) {
	            sawWS = true;
	            stripWhitespace(1);
	        }
	        else if (firstChar in Traversals) {
	            tokens.push({ type: Traversals[firstChar] });
	            sawWS = false;
	            stripWhitespace(1);
	        }
	        else if (firstChar === ",") {
	            if (tokens.length === 0) {
	                throw new Error("Empty sub-selector");
	            }
	            subselects.push(tokens);
	            tokens = [];
	            sawWS = false;
	            stripWhitespace(1);
	        }
	        else {
	            if (sawWS) {
	                if (tokens.length > 0) {
	                    tokens.push({ type: "descendant" });
	                }
	                sawWS = false;
	            }
	            if (firstChar === "*") {
	                selector = selector.substr(1);
	                tokens.push({ type: "universal" });
	            }
	            else if (firstChar in attribSelectors) {
	                var _a = attribSelectors[firstChar], name_1 = _a[0], action = _a[1];
	                selector = selector.substr(1);
	                tokens.push({
	                    type: "attribute",
	                    name: name_1,
	                    action: action,
	                    value: getName(),
	                    ignoreCase: false
	                });
	            }
	            else if (firstChar === "[") {
	                selector = selector.substr(1);
	                var data = selector.match(reAttr);
	                if (!data) {
	                    throw new Error("Malformed attribute selector: " + selector);
	                }
	                selector = selector.substr(data[0].length);
	                var name_2 = unescapeCSS(data[1]);
	                if (!options ||
	                    ("lowerCaseAttributeNames" in options
	                        ? options.lowerCaseAttributeNames
	                        : !options.xmlMode)) {
	                    name_2 = name_2.toLowerCase();
	                }
	                tokens.push({
	                    type: "attribute",
	                    name: name_2,
	                    action: actionTypes[data[2]],
	                    value: unescapeCSS(data[4] || data[5] || ""),
	                    ignoreCase: !!data[6]
	                });
	            }
	            else if (firstChar === ":") {
	                if (selector.charAt(1) === ":") {
	                    selector = selector.substr(2);
	                    tokens.push({
	                        type: "pseudo-element",
	                        name: getName().toLowerCase()
	                    });
	                    continue;
	                }
	                selector = selector.substr(1);
	                var name_3 = getName().toLowerCase();
	                var data = null;
	                if (selector.charAt(0) === "(") {
	                    if (unpackPseudos.has(name_3)) {
	                        var quot = selector.charAt(1);
	                        var quoted = quotes.has(quot);
	                        selector = selector.substr(quoted ? 2 : 1);
	                        data = [];
	                        selector = parseSelector(data, selector, options);
	                        if (quoted) {
	                            if (selector.charAt(0) !== quot) {
	                                throw new Error("Unmatched quotes in :" + name_3);
	                            }
	                            else {
	                                selector = selector.substr(1);
	                            }
	                        }
	                        if (selector.charAt(0) !== ")") {
	                            throw new Error("Missing closing parenthesis in :" + name_3 + " (" + selector + ")");
	                        }
	                        selector = selector.substr(1);
	                    }
	                    else {
	                        var pos = 1, counter = 1;
	                        for (; counter > 0 && pos < selector.length; pos++) {
	                            if (selector.charAt(pos) === "(" && !isEscaped(pos))
	                                counter++;
	                            else if (selector.charAt(pos) === ")" &&
	                                !isEscaped(pos))
	                                counter--;
	                        }
	                        if (counter) {
	                            throw new Error("Parenthesis not matched");
	                        }
	                        data = selector.substr(1, pos - 2);
	                        selector = selector.substr(pos);
	                        if (stripQuotesFromPseudos.has(name_3)) {
	                            var quot = data.charAt(0);
	                            if (quot === data.slice(-1) && quotes.has(quot)) {
	                                data = data.slice(1, -1);
	                            }
	                            data = unescapeCSS(data);
	                        }
	                    }
	                }
	                tokens.push({ type: "pseudo", name: name_3, data: data });
	            }
	            else if (reName.test(selector)) {
	                var name_4 = getName();
	                if (!options ||
	                    ("lowerCaseTags" in options
	                        ? options.lowerCaseTags
	                        : !options.xmlMode)) {
	                    name_4 = name_4.toLowerCase();
	                }
	                tokens.push({ type: "tag", name: name_4 });
	            }
	            else {
	                if (tokens.length &&
	                    tokens[tokens.length - 1].type === "descendant") {
	                    tokens.pop();
	                }
	                addToken(subselects, tokens);
	                return selector;
	            }
	        }
	    }
	    addToken(subselects, tokens);
	    return selector;
	}
	function addToken(subselects, tokens) {
	    if (subselects.length > 0 && tokens.length === 0) {
	        throw new Error("Empty sub-selector");
	    }
	    subselects.push(tokens);
	}
	});

	unwrapExports(parse_1);

	var stringify_1 = createCommonjsModule(function (module, exports) {
	Object.defineProperty(exports, "__esModule", { value: true });
	var actionTypes = {
	    equals: "",
	    element: "~",
	    start: "^",
	    end: "$",
	    any: "*",
	    not: "!",
	    hyphen: "|"
	};
	var simpleSelectors = {
	    child: " > ",
	    parent: " < ",
	    sibling: " ~ ",
	    adjacent: " + ",
	    descendant: " ",
	    universal: "*"
	};
	function stringify(token) {
	    return token.map(stringifySubselector).join(", ");
	}
	exports.default = stringify;
	function stringifySubselector(token) {
	    return token.map(stringifyToken).join("");
	}
	function stringifyToken(token) {
	    if (token.type in simpleSelectors)
	        return simpleSelectors[token.type];
	    if (token.type === "tag")
	        return escapeName(token.name);
	    if (token.type === "pseudo-element")
	        return "::" + escapeName(token.name);
	    if (token.type === "attribute") {
	        if (token.action === "exists") {
	            return "[" + escapeName(token.name) + "]";
	        }
	        if (token.name === "id" &&
	            token.action === "equals" &&
	            !token.ignoreCase) {
	            return "#" + escapeName(token.value);
	        }
	        if (token.name === "class" &&
	            token.action === "element" &&
	            !token.ignoreCase) {
	            return "." + escapeName(token.value);
	        }
	        var atributeName = escapeName(token.name);
	        var action = actionTypes[token.action];
	        var value = escapeName(token.value);
	        var ignoreCase = token.ignoreCase ? "i" : "";
	        return "[" + atributeName + action + "='" + value + "'" + ignoreCase + "]";
	    }
	    if (token.type === "pseudo") {
	        if (token.data === null)
	            return ":" + escapeName(token.name);
	        if (typeof token.data === "string") {
	            return ":" + escapeName(token.name) + "(" + token.data + ")";
	        }
	        return ":" + escapeName(token.name) + "(" + stringify(token.data) + ")";
	    }
	    throw new Error("Unknown type");
	}
	function escapeName(str) {
	    //TODO
	    return str;
	}
	});

	unwrapExports(stringify_1);

	var lib = createCommonjsModule(function (module, exports) {
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	__export(parse_1);
	var parse_1$1 = parse_1;
	exports.parse = parse_1$1.default;

	exports.stringify = stringify_1.default;
	});

	unwrapExports(lib);
	var lib_1 = lib.parse;
	var lib_2 = lib.stringify;

	let nextFiberID = 1;
	const fiberIDMap = new WeakMap();

	const FunctionComponent = 0;
	const ClassComponent = 1;
	const IndeterminateComponent = 2; // Before we know whether it is function or class
	const HostRoot = 3; // Root of a host tree. Could be nested inside another node.
	const HostPortal = 4; // A subtree. Could be an entry point to a different renderer.
	const HostComponent = 5;
	const HostText = 6;
	const Fragment = 7;
	const Mode = 8;
	const ContextConsumer = 9;
	const ContextProvider = 10;
	const ForwardRef = 11;
	const Profiler = 12;
	const SuspenseComponent = 13;
	const MemoComponent = 14;
	const SimpleMemoComponent = 15;
	const LazyComponent = 16;
	const IncompleteClassComponent = 17;
	const DehydratedFragment = 18;
	const SuspenseListComponent = 19;
	const FundamentalComponent = 20;
	const ScopeComponent = 21;
	const Block = 22;

	function getFiberUniqueID(fiber) {
	  if (!fiberIDMap.has(fiber)) {
	    fiberIDMap.set(fiber, nextFiberID++);
	  }
	  return fiberIDMap.get(fiber);
	}

	function getFriendlyTag(tag) {
	  switch (tag) {
	    case IndeterminateComponent:
	      return '[indeterminate]';
	    case FunctionComponent:
	      return '[fn]';
	    case ClassComponent:
	      return '[class]';
	    case HostRoot:
	      return '[root]';
	    case HostPortal:
	      return '[portal]';
	    case HostComponent:
	      return '[host]';
	    case HostText:
	      return '[text]';
	    case Fragment:
	      return '[fragment]';
	    case Mode:
	      return '[mode]';
	    case ContextConsumer:
	      return '[consumer]';
	    case ContextProvider:
	      return '[provider]';
	    case ForwardRef:
	      return '[forwardref]'
	    case Profiler:
	      return '[profiler]';
	    case SuspenseComponent:
	      return '[suspense]';
	    case MemoComponent:
	      return '[memo]';
	    case SimpleMemoComponent:
	      return '[simplememo]'
	    case LazyComponent:
	      return '[lazy]'
	    case IncompleteClassComponent:
	      return '[incompleteclasscomponent]'
	    case DehydratedFragment:
	      return '[dehydrated]'
	    case SuspenseListComponent:
	      return '[suspenselist]'
	    case FundamentalComponent:
	      return '[fundamentalcomponent]'
	    case ScopeComponent:
	      return '[scopecomponent]'
	    case Block:
	      return '[block]'
	    default:
	      throw new Error('Unknown tag.');
	  }
	}

	function getFriendlyEffect(effectTag) {
	  const effects = {
	    1: 'Performed Work',
	    2: 'Placement',
	    4: 'Update',
	    8: 'Deletion',
	    16: 'Content reset',
	    32: 'Callback',
	    64: 'Err',
	    128: 'Ref',
	  };
	  return Object.keys(effects)
	    .map(Number)
	    .filter(flag => flag & effectTag)
	    .map(flag => effects[flag])
	    .join(' & ');
	}

	function describeFibers(rootFiber, workInProgress) {
	  let descriptions = {};
	  function acknowledgeFiber(fiber) {
	    if (!fiber) {
	      return null;
	    }
	    if (!fiber.return && fiber.tag !== 3) {
	      return null;
	    }
	    const id = getFiberUniqueID(fiber);
	    if (descriptions[id]) {
	      return id;
	    }

	    descriptions[id] = {};
	    Object.assign(descriptions[id], {
	      ...fiber,
	      _desc: {
	        id: id,
	        tag: getFriendlyTag(fiber.tag),
	        effectTag: getFriendlyEffect(fiber.effectTag),
	        type: fiber.type && (fiber.type.name || fiber.type.toString()),
	        stateNode: `[${typeof fiber.stateNode}]`,
	        return: acknowledgeFiber(fiber.return),
	        child: acknowledgeFiber(fiber.child),
	        sibling: acknowledgeFiber(fiber.sibling),
	        nextEffect: acknowledgeFiber(fiber.nextEffect),
	        firstEffect: acknowledgeFiber(fiber.firstEffect),
	        lastEffect: acknowledgeFiber(fiber.lastEffect),
	        alternate: acknowledgeFiber(fiber.alternate),
	      }
	    });
	    return id;
	  }

	  const rootID = acknowledgeFiber(rootFiber);
	  const workInProgressID = acknowledgeFiber(workInProgress);

	  let currentIDs = [];
	  function markAsCurrent(id) {
	    currentIDs.push(id);
	    const fiber = descriptions[id];
	    if (!fiber) {
	      console.log(descriptions);
	      throw new Error(`Could not find fiber ${id}`)
	    }

	    if (fiber.child) {
	      markAsCurrent(fiber.child);
	    }

	    if (fiber.sibling) {
	      markAsCurrent(fiber.sibling);
	    }
	  }
	  markAsCurrent(rootID);

	  return {
	    descriptions,
	    rootID,
	    currentIDs,
	    workInProgressID,
	  };
	}

	const findDOMComponent = (node) => {
	  if (node.tag === HostComponent) {
	    return node.stateNode;
	  }

	  if (node.child) {
	    if (node.child.tag === HostComponent) {
	      return node.child.stateNode;
	    }

	    return findDOMComponent(node.child);
	  }
	};

	var Utils = {
	  findDOMComponent,
	  isRootNode: (node) => {
	    return node._reactRootContainer
	      && node._reactRootContainer._internalRoot
	      && node._reactRootContainer._internalRoot.current;
	  },
	  isReactNode: (node) => {
	    return Object.keys(node).find(key => key.startsWith('__reactInternalInstance'))
	  }
	};

	function encode(query) {
	  return query.replace('.', '_____')
	}

	function decode(query) {
	  return query.replace('_____', '.')
	}

	function parseQuery(query) {
	  // Ignore select by css (so we can use name with '.')
	  query = encode(query);
	  return lib_1(query, { lowerCaseAttributeNames: false, lowerCaseTags: false });
	}

	// Current design is not intended to be optimized or fast per-ser. We'll always travel the whole tree to find
	const handlers = {
	  'tag': ({ name }, sources) => {
	    // const { currentIDs, descriptions } = describeFibers(node);
	    let result = [];
	    name = decode(name);

	    function matchByName(fiber) {
	      return fiber.type && (
	        fiber.type.name === name ||
	        fiber.type.displayName === name ||
	        fiber.type.toString() === name)
	    }

	    function findIn(fiber) {
	      if (matchByName(fiber)) {
	        result.push(fiber);
	      } else {
	        fiber.child && findIn(fiber.child);
	      }

	      fiber.sibling && findIn(fiber.sibling);
	    }

	    sources.forEach(findIn);
	    return result;
	  },
	  'pseudo': ({ name, data }, sources) => {
	    if (sources.length === 0) return sources;

	    switch (name) {
	      case 'first': return [sources[0]]
	      case 'last': return [sources[sources.length - 1]]
	      case 'skip': return sources.slice(data)
	      case 'nth': return sources[data] ? [sources[data]] : []
	      default: throw Error(`:${name} is unsupported`)
	    }
	  },
	  'attribute': ({ name, action, value }, sources) => {
	    let result = [];
	    value = value.toString();

	    for (let source of sources) {
	      const memoizedProps = source.memoizedProps;
	      switch (action) {
	        case 'exists': memoizedProps[name] && result.push(source); break;
	        case 'equals': `${memoizedProps[name]}` === value && result.push(source); break;
	        case 'start': `${memoizedProps[name]}`.startsWith(value) && result.push(source); break;
	        case 'end': `${memoizedProps[name]}`.endsWith(value) && result.push(source); break;
	        default: throw Error(`[${action}] is an unsupported`)
	      }
	    }
	    return result;
	  },
	  'child': (_, sources) => {
	    let result = [];
	    function findSibling(fiber) {
	      if (fiber.sibling) {
	        result.push(fiber.sibling);
	        findSibling(fiber.sibling);
	      }
	    }

	    for (let source of sources) {
	      if (source.child) {
	        result.push(source.child);
	        findSibling(source.child);
	      }
	    }

	    return result
	  },
	  'parent': (_, sources) => {
	    return sources.map(source => source.return)
	  },
	  'sibling': (_, sources) => {
	    let result = [];

	    function findSibling(fiber) {
	      if (fiber.sibling) {
	        result.push(fiber.sibling);
	        findSibling(fiber.sibling);
	      }
	    }

	    for (let source of sources) {
	      findSibling(source);
	    }
	    return result;
	  },
	  'adjacent': (_, sources) => {
	    let result = [];
	    for (let source of sources) {
	      if (source.sibling) result.push(source.sibling);
	    }
	    return result;
	  },
	  'descendant': (_, sources) => handlers['child'](_, sources)
	};


	function _query(queries, node) {
	  let result = [node];

	  for (let query of queries) {
	    result = [...new Set(handlers[query.type](query, result))];

	  }

	  return result.map(child => Utils.findDOMComponent(child));
	}

	function query(rootNode, queryString, debug) {
	  if (!rootNode) {
	    throw new Error("rootNode is not given")
	  }

	  if (!queryString || typeof queryString != 'string') {
	    throw new Error("queryString must be a string")
	  }

	  if (!Utils.isRootNode(rootNode) && !Utils.isReactNode(rootNode)) {
	    throw new Error("given rootNode is not a React node");
	  }

	  const parsedQuery = parseQuery(queryString)[0];
	  const reactNode = Utils.isRootNode(rootNode) || Utils.isReactNode(rootNode);

	  return _query(parsedQuery, reactNode);
	}

	var index = {
	  query, describeFibers
	};

	return index;

}());
