import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from "rollup-plugin-terser";

export default {
  input: 'lib/index.js',
  output: [
    { file: "dist/react-query.iife.js", format: "iife", name: "rquery" },
    { file: "dist/react-query.cjs.js", format: "cjs", plugins: [terser()] }
  ],
  plugins: [
    commonjs(),
    resolve()
  ]
};