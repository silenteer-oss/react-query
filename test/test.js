import test from 'ava'
import TodoPage from './fixture'
import React from 'react'

import query from '../dist/react-query.cjs'
import TestRenderer from 'react-test-renderer'

const testRenderer = TestRenderer.create(<TodoPage />)
const fiber = testRenderer.root._fiber
const fakeRoot = { _reactRootContainer: { _internalRoot: { current: fiber } } }
const testQuery = (q) => query.query(fakeRoot, q)

test('react-query', t => {
  t.is(testQuery('TodoPage').length, 1, 'Query by class component should work')
  t.is(testQuery('TodoPage')[0].type, 'div', 'Query by class component should work')

  t.is(testQuery('li').length, 5)
  t.is(testQuery('TodoItem').length, 4)

  t.is(testQuery('TodoItem li button').length, 4)
  t.is(testQuery('TodoItem > li button:nth(1)').length, 1)
  t.is(testQuery('TodoItem > li button:nth(1)')[0].type, 'button')
  t.is(testQuery('ul li:first').length, 1)
  t.is(testQuery('ul li:last').length, 1)
  t.is(testQuery('button[value]').length, 5)
  t.is(testQuery('button[value^=Remove]').length, 4)
  t.is(testQuery(`button[value^='Remove']`).length, 4)
  t.is(testQuery(`button[value^='Remove 2']`).length, 1)
  t.is(testQuery(`button[value$=2]`).length, 1)
  t.is(testQuery('input + button <').length, 1)
  t.is(testQuery('input + button <')[0].type, 'form')
  t.is(testQuery('button').length, 5)
  t.is(testQuery('TodoList button').length, 4)
  t.is(testQuery('HOC').length, 1)

  t.is(testQuery('li[data-test-id=1]').length, 1)
})
