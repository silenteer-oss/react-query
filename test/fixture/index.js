import React from 'react'

let todoList = [
  { id: 1, text: 'react' },
  { id: 2, text: 'query' },
  { id: 3, text: 'is' },
  { id: 4, text: 'awesome' },
]

const { Consumer, Provider } = React.createContext(todoList)

const HOC = ({ children }) => {
  return <div>{children}</div>
}

// Component should be a mixture of function (arrow and non-arrow) and class component
class TodoPage extends React.Component {
  render() {
    return <>
      <div>
        <AddTodoComponent />
        <HOC><TodoList /></HOC>
      </div>
    </>
  }
}

function TodoList() {
  return <ul>
    <li><span>Todo</span><span>Remove</span></li>
    {todoList.map(todo => <Namespace.TodoItem key={todo.id} {...todo} />)}

  </ul>
}

const Namespace = {
  TodoItem: ({ id, text }) => {
    return <li key={id} data-test-id={id}>{text} <button value={`Remove ${id}`} /></li>
  }
}

const AddTodoComponent = () =>
  <form>
    <input name="text" />
    <button value="Submit" />
  </form>

export default TodoPage